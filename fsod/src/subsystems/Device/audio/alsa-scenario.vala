/* 
 * alsa-scenario.vala
 * Written by Sudharshan "Sup3rkiddo" S <sudharsh@gmail.com>
 * All Rights Reserved
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */ 


using DBus;
using ODeviced;
using Gst;
using FSOD;
using Device;



public class AlsaScenario : GLib.Object {

	private GLib.List<string> stack = new GLib.List<string>();
	private string current = "unknown";


	public Audio dbus_object {
		get;
		construct;
	}

	public string statedir {
		get;
		construct;
	}

	public string defaultscenario {
		get;
		construct;
	}

	public AlsaScenario(Audio dbus_object, string statedir, string defaultscenario) {
		this.dbus_object = dbus_object;
		this.statedir = statedir;
		this.defaultscenario = defaultscenario;
	}

	construct {
		GLib.Idle.add(this.initScenario);
		log ("Device.Audio", LogLevelFlags.LEVEL_INFO, "Starting Audio subsystem. Statedir = %s, Default Scenario = %s",
			 this.statedir, this.defaultscenario);
	}


	private bool initScenario() {
		if (!FileUtils.test (this.statedir + "/" +
							 this.defaultscenario + ".state", FileTest.EXISTS)
			&& setScenario(this.defaultscenario)) {

			log ("Device.Audio", LogLevelFlags.LEVEL_INFO, "Default ALSA scenario restored successfully");
		}
		else
			log ("Device.Audio", LogLevelFlags.LEVEL_INFO | LogLevelFlags.LEVEL_WARNING,
				 "Default scenario %s not found", this.defaultscenario);
		return false; /* This should be executed only once */
	}


	public bool pushScenario(string scenario) {
		string current = this.current;
		if (this.setScenario(scenario)) {
			this.stack.prepend(current);
			return true;
		}
		return false;
	}


	public string getScenario() {
		return this.current;
	}


	public string? pullScenario() {
		string previous = this.stack.nth_data(0);
		this.stack.remove(previous);
		if (setScenario(previous))
			return previous;
		return null;
	}
				

	[CCode (array_length = false, array_null_terminated = true)]		
	public string[] getScenarios() {

		string[] scenarios = null;
		string scenario_file = null;
		Dir dir = Dir.open(this.statedir);
		int i = 0;
		while ( (scenario_file = dir.read_name()) != null ) {
			if (scenario_file.has_suffix(".state"))
				scenarios[i] = scenario_file.split(".")[1];
			i++;
		}
		return scenarios;
	}
			

	public bool setScenario(string scenario) {
		if (!call_alsactl("restore", scenario)) {
			log("Device.Audio", LogLevelFlags.LEVEL_INFO | LogLevelFlags.LEVEL_WARNING,
				"SetScenario %s failed", scenario);
			return false;
		}
		this.current = scenario;
		dbus_object.scenario(scenario, "user");
		return true;
	}


	public bool storeScenario(string scenario) {

		if (!call_alsactl("store", scenario)) {
			log("Device.Audio", LogLevelFlags.LEVEL_INFO | LogLevelFlags.LEVEL_WARNING,
				"StoreScenario %s failed", scenario);
			return false;
		}
		log("Device.Audio", LogLevelFlags.LEVEL_INFO | LogLevelFlags.LEVEL_WARNING,
			"StoreScenario %s successful", scenario);
		return true;
	}


	private bool call_alsactl(string mode, string scenario) {

		string[] scenarios = this.getScenarios();
		string statename = Path.build_filename(this.statedir, scenario);
		if (!(has_string(scenarios, scenario)))
			return false;
		string[] alsactl_argv = { "alsactl", "-f", statename, mode };
		try {
			Process.spawn_sync(null, alsactl_argv, null, SpawnFlags.SEARCH_PATH,
						  null, null);
			log ("Device.Audio", LogLevelFlags.LEVEL_INFO, "Scenario %s set successfully", scenario);
						
		}
		catch (SpawnError error) {
			log ("Device.Audio", LogLevelFlags.LEVEL_INFO | LogLevelFlags.LEVEL_WARNING,
				 "alsactl couldn't be called: %s", error.message);
			return false;
		}
		return true;
	}

		

}
	

/* Simple linear search */		
bool has_string(string[] array, string key) {
	int i = 0;
	if (array == null)
		return false;
	while (array[i] != null) {
		if (array[i] == key)
			return true;
		i++;
	}
	
	return false;
}
